import React, { Component } from 'react';
import { StyleSheet, View, TextInput, TouchableOpacity} from 'react-native';
import Icon from 'react-native-vector-icons/EvilIcons';

class SearchBar extends Component {
  constructor(props) {
      super(props);
      this.state = { text: 'Search' };
    }

  render() {
    return (
      <View style={styles.container}>
        <TouchableOpacity style={styles.chevronButton}>
          <Icon name="chevron-left" size={60} />
        </TouchableOpacity>
        <TextInput
          style={styles.searchInput}
          onChangeText={(text) => this.setState({text})}
          placeholder={this.state.text}
        />
      </View>
    );
  }
}

export default SearchBar;

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    marginRight: 20,
    marginBottom: 10,
    height: 60
  },
  chevronButton: {
    height: 60,
    width: 60,
    alignItems: 'center',
    justifyContent: 'center'
  },
  searchInput: {
    height: 60,
    flex: 1,
    borderWidth: 2,
    borderRadius: 30,
    borderColor: '#9B9B9B',
    alignSelf: 'stretch',
    paddingTop: 10,
    paddingBottom: 10,
    paddingLeft: 30
  }
});
