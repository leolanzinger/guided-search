import React, { Component } from 'react';
import { StyleSheet, View, Text, TextInput, TouchableOpacity, LayoutAnimation} from 'react-native';
import SuggestionPictures from '../components/SuggestionPictures';

/*
  Fake GET request for data
*/
const suggestionsJson = require('../mocks/suggestions.json');

/*
  Custom spring animation that handles layout changes
*/
const CustomSpring = {
    duration: 400,
    create: {
      type: LayoutAnimation.Types.spring,
      property: LayoutAnimation.Properties.scaleXY,
      springDamping: 0.7,
    },
    update: {
      type: LayoutAnimation.Types.spring,
      springDamping: 0.7,
    },
  };

class SearchSuggestions extends Component {
  constructor(props) {
    super(props);
    this.state = {suggestions: [],
    show: true,
    active: [],
    activeId: null
    };
  };

  /*
    Load data from network request and populate suggestions array
  */
  componentDidMount() {
    const activeArray = Array.from(suggestionsJson.suggestions, () => false);
    this.setState({
      suggestions: suggestionsJson.suggestions,
      active: activeArray
    });
  }

  /*
    Trigger layout change animation.
    Update component state and set active suggestion button.
  */
  handleClick(id) {
    LayoutAnimation.configureNext(CustomSpring);
    this.setState({activeId: id});
    const act = this.state.active;
    if (this.state.show === true) {
      act[id] = true;
      this.setState({show: false, active: act})
    }
    else {
      act[id] = false;
      this.setState({show: true, active: act})
    }
  };

  render() {
    {/*
      Renders suggestion buttons array from state props.
    */}
    const buttonsArr = this.state.suggestions.map(button => (
      (
        this.state.show ||
        this.state.active[button.id]
      ) &&
      <TouchableOpacity
        style={[
          styles.suggestionButton,
          this.state.active[button.id] && styles.activeSuggestionButton
        ]}
        key={button.id}
        onPress={() => this.handleClick(button.id)}
      >
        <Text style={[
          styles.suggestionButtonText,
          this.state.active[button.id] && styles.activeSuggestionButtonText
        ]}>
          {button.text}
        </Text>
      </TouchableOpacity>
    ));
    return (
      <View style={styles.biggerContainer}>
        <View style={styles.topContainer}>
          { this.state.show && <Text style={styles.title}>Search Suggestions</Text>}
          <View style={styles.buttonsContainer}>
            {buttonsArr}
          </View>
        </View>
        {/*
          DOM renders only if suggestion is active
        */}
        { !this.state.show &&
          <View style={styles.suggestionsContainer}>
            <Text style={styles.title}>Search Suggestions</Text>
              <SuggestionPictures pics={suggestionsJson.suggestions[this.state.activeId].images}/>
          </View>
        }
      </View>
    )
  };
};

export default SearchSuggestions;

const styles = StyleSheet.create({
  biggerContainer: {
    flex: 1,
    justifyContent: 'flex-start',
    alignItems: 'stretch'
  },
  topContainer: {
    paddingLeft: 20,
    paddingRight: 20,
    flex: 1
  },
  buttonsContainer: {
    marginTop: 15,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
    flexWrap: 'wrap'
  },
  suggestionsContainer: {
    backgroundColor: '#FFFFFF',
    marginTop: 10,
    paddingTop: 10,
    paddingBottom: 10,
    paddingLeft: 20,
    paddingRight: 20,
    flex: 1,
    shadowColor: '#000000',
    shadowOffset: {
      width: 0,
      height: -1
    },
    shadowRadius: 2,
    shadowOpacity: 0.3,
  },
  title: {
    fontWeight: '800',
    fontSize: 20,
    paddingTop: 15
  },
  suggestionButton: {
    borderColor: '#9B9B9B',
    paddingTop: 10,
    paddingBottom: 10,
    paddingLeft: 15,
    paddingRight: 15,
    borderRadius: 30,
    marginRight: 10,
    marginBottom: 10,
    borderWidth: 2
  },
  activeSuggestionButton: {
    borderColor: '#FF6900',
    backgroundColor: '#FF6900',
    paddingTop: 10,
    paddingBottom: 10,
    paddingLeft: 15,
    paddingRight: 15,
    borderRadius: 30,
    marginRight: 10,
    borderWidth: 2
  },
  suggestionButtonText: {
    fontSize: 18
  },
  activeSuggestionButtonText: {
    fontSize: 18,
    color: '#FFFFFF',
    fontWeight: '600'
  }
});
