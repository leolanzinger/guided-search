import React, { Component } from 'react';
import { StyleSheet, View, TouchableOpacity, Image } from 'react-native';

class SuggestionPictures extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    const images = this.props.pics.map(pic => (
      <TouchableOpacity key={pic.id}>
        <Image
          resizeMode="cover"
          style={styles.image}
          source={{uri: pic.url}}
        >
        </Image>
      </TouchableOpacity>
    ));

    return(
      <View style={styles.picContainer}>
        { images }
      </View>
    );
  }
};

export default SuggestionPictures;

const styles = StyleSheet.create({
  picContainer: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'flex-start',
    flexWrap: 'wrap',
    marginTop: 20
  },
  image: {
    width: 130,
    height: 160,
    marginBottom: 20
  }
});
