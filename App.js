import React from 'react';
import { StyleSheet, Text, View, ScrollView } from 'react-native';
import SearchBar from './app/components/SearchBar';
import SearchSuggestions from './app/components/SearchSuggestions';

export default class App extends React.Component {
  render() {
    return (
      <ScrollView style={{backgroundColor: '#FFFFFF'}}>
        <View style={styles.container}>
          <SearchBar />
          <SearchSuggestions />
        </View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 40,
    justifyContent: 'flex-start',
    alignItems: 'stretch'
  },
});
